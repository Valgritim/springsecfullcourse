package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecFullCourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecFullCourseApplication.class, args);
	}

}
