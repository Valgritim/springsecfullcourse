package com.example.demo.jwt;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JwtTokenVerifier extends OncePerRequestFilter{


	@SuppressWarnings("deprecation")
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// Je vais récupérer le token qui est présent dans le Header, paramètre que j'ai appelé "authorization"
		String authorizationHeader = request.getHeader("authorization");
		
		//1 . si le parametre est vide ou s'il ne commence pas par Bearer:, la requete sera rejetée
		if(authorizationHeader.toString().isEmpty() || !authorizationHeader.startsWith("Bearer:")) {
			filterChain.doFilter(request, response);
			return;
		}
		
		//2. Le param n'est pas vide,
		String token = authorizationHeader.replace("Bearer:", "");
		try {
			
			String secretKey = "myverysecretkeykeykeykeykeykeykey";
			
			//1. Je vais chercher le token dans le header
			Jws<Claims> claimsJws = Jwts.parser()
				.setSigningKey(Keys.hmacShaKeyFor(secretKey.getBytes()))
				.parseClaimsJws(token); //Jwt est appelé Jws parce le Jwt a été signé et compacté
			
			//2. Je vais chercher le body(PayLoad)
			 Claims body = claimsJws.getBody();
			 String username = body.getSubject(); // Le subject est le username
			 
			 @SuppressWarnings("unchecked")
			List<Map<String, String>> authorities = (List<Map<String, String>>) body.get("authorities");
			 
			 //Je récupère mes authorities dans une liste: Mais : Authority a besoin d'etre une collection qui extends GrantedAuthority (Rappel: public Collection<? extends GrantedAuthority> getAuthorities())
			 Set<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
			 			.map(m -> new SimpleGrantedAuthority(m.get("authority")))
			 			.collect(Collectors.toSet());
			 
			 Authentication authentication = new UsernamePasswordAuthenticationToken(
					 username,
					 null,
					 simpleGrantedAuthorities
					 );
			 
			 SecurityContextHolder.getContext().setAuthentication(authentication);;
			 
		} catch (JwtException e) {
			throw new IllegalStateException(String.format("Token %s cannot be trusted", token)); //signifie que le token a été modifié ou a expiré
		}
		//je passe la requete et la reponse au prochain filter
		filterChain.doFilter(request, response);
	}

}
