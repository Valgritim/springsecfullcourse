package com.example.demo.jwt;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	
	
	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
		super();
		this.authenticationManager = authenticationManager;
	}


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException{
		
	
		//Je vais lire la valeur des champs et je le map à ma classe ci-dessous	
		
				try {
					
					//1.L'objectMapper va lire la valeur de l'input et le mettre dans une instance de la classe UsernameAndPasswordAuthenticationRequest
					UsernameAndPasswordAuthenticationRequest authenticationRequest = new ObjectMapper().readValue(request.getInputStream(), UsernameAndPasswordAuthenticationRequest.class);
					
					//2. je construis mon authentification avec le username et le password
					Authentication authentication = new UsernamePasswordAuthenticationToken(
							authenticationRequest.getUsername(), //le username représente le principle
							authenticationRequest.getPassword() //le password représente les credentials
					);	
					
					//3. le authenticationmanager vérifie que le username existe et si le pwd est correct ou non puis valide l'authentification
					Authentication authenticate = authenticationManager.authenticate(authentication);
					return authenticate;

				} catch (IOException e) {
					// TODO Auto-generated catch block
					throw new RuntimeException(e);
				}	
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		
		String secretKey = "myverysecretkeykeykeykeykeykeykey";
		String token = Jwts.builder()
				.setSubject(authResult.getName())
				.claim("authorities", authResult.getAuthorities())
				.setIssuedAt(new java.util.Date())
				.setExpiration(java.sql.Date.valueOf(LocalDate.now().plusWeeks(2)))
				.signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
				.compact();
		
		response.addHeader("authorization", "Bearer:" + token);
	}
}
