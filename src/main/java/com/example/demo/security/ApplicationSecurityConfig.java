package com.example.demo.security;


import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import com.example.demo.auth.ApplicationUserService;
import com.example.demo.jwt.JwtTokenVerifier;
import com.example.demo.jwt.JwtUsernameAndPasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{
	
	private final PasswordEncoder passwordEncoder;
	private final ApplicationUserService applicationUserService;
	
	//constructeur
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, ApplicationUserService applicationUserService) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.applicationUserService = applicationUserService;
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		//http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
		http.csrf().disable()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager())) //on peut appeler la methode de WebSecurityConfigurerAdapter
			.addFilterAfter(new JwtTokenVerifier(), JwtUsernameAndPasswordAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers("/","index","/css/*","/js/*")
			.permitAll()
			.antMatchers("/api/**").hasRole(ApplicationUserRole.STUDENT.name())
//			.antMatchers(HttpMethod.DELETE, "/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
			.anyRequest()
			.authenticated();
		

	}
	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception{
//		//http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
//		http.csrf().disable();
//		http.authorizeRequests()
//			.antMatchers("/","index","/css/*","/js/*")
//			.permitAll()
//			.antMatchers("/api/**").hasRole(ApplicationUserRole.STUDENT.name())
////			.antMatchers(HttpMethod.DELETE, "/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
//			.anyRequest()
//			.authenticated()
//			.and()
//			//.httpBasic();
//			.formLogin()
//				.loginPage("/login").permitAll()
//				.defaultSuccessUrl("/courses", true)
//				.usernameParameter("username") //correspond au nom de l'input pour le username
//				.passwordParameter("password")// correspond au nom de l'input pour le password
//			.and()
//			.rememberMe().tokenValiditySeconds((int)TimeUnit.DAYS.toSeconds(21))
//						 .key("somethingverysecure")
//						 .rememberMeParameter("remember-me")// correspond au nom de l'input pour le remember me
//			.and()
//			.logout()
//				.logoutUrl("/logout")
//				.clearAuthentication(true)
//				.invalidateHttpSession(true)
//				.deleteCookies("JSESSIONID","remember-me")
//				.logoutSuccessUrl("/login");
//	}

//	@Override
//	@Bean
//	protected UserDetailsService userDetailsService() {
//		
//		UserDetails annaSmithUser = User.builder()
//			.username("annasmith")
//			.password(passwordEncoder.encode("password"))
////			.roles(ApplicationUserRole.STUDENT.name()) //ROLE_STUDENT
//			.authorities(ApplicationUserRole.STUDENT.getGrantedAuthorities())
//			.build();
//		
//		UserDetails lindaUser =User.builder()
//			.username("linda")
//			.password(passwordEncoder.encode("password123"))
////			.roles(ApplicationUserRole.ADMIN.name()) //ROLE_ADMIN
//			.authorities(ApplicationUserRole.ADMIN.getGrantedAuthorities())
//			.build();
//		
//		UserDetails tomUser =User.builder()
//				.username("tom")
//				.password(passwordEncoder.encode("password123"))
////				.roles(ApplicationUserRole.ADMINTRAINEE.name()) //ROLE_ADMINTRAINEE
//				.authorities(ApplicationUserRole.ADMINTRAINEE.getGrantedAuthorities())
//				.build();
//		
//		return new InMemoryUserDetailsManager(annaSmithUser, lindaUser, tomUser);
//	}
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setPasswordEncoder(passwordEncoder);
		authenticationProvider.setUserDetailsService(applicationUserService);
		
		return authenticationProvider;
	}
}
