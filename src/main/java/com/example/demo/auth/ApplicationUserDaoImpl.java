package com.example.demo.auth;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.example.demo.security.ApplicationUserRole;
import com.google.common.collect.Lists;

@Repository("fake")
public class ApplicationUserDaoImpl implements ApplicationUserDao{

	private final PasswordEncoder passwordEncoder;
	
	@Autowired
	public ApplicationUserDaoImpl(PasswordEncoder passwordEncoder) {
		super();
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
	
		return getApplicationUser()
					.stream()
					.filter(applicationUser -> username.equals(applicationUser.getUsername()))
					.findFirst();
	}

	private List<ApplicationUser> getApplicationUser(){
		
		List<ApplicationUser> applicationUsers = Lists.newArrayList(
				new ApplicationUser(
						passwordEncoder.encode("password"),
						"annasmith",
						ApplicationUserRole.STUDENT.getGrantedAuthorities(),
						true,
						true,
						true,
						true
						),
				new ApplicationUser(
						
						passwordEncoder.encode("password"),
						"linda",
						ApplicationUserRole.ADMIN.getGrantedAuthorities(),
						true,
						true,
						true,
						true
						),
				new ApplicationUser(
						passwordEncoder.encode("password"),
						"tom",
						ApplicationUserRole.ADMINTRAINEE.getGrantedAuthorities(),
						true,
						true,
						true,
						true
						)
				);
		
		return applicationUsers;
	}
}
